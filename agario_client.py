# IMPORTANDO E INICIALIZANDO MÓDULOS NECESSÁRIOS
import pygame, sys, random, time, socket
from pygame.locals import *
pygame.init()

# CONFIGURANDO A TELA
SCREEN = pygame.display.set_mode((844, 609), pygame.RESIZABLE)
fundo= pygame.image.load("FD.png")
SCREEN.blit(fundo,(0, 0))
FPS = 15
CLOCK = pygame.time.Clock()
pygame.display.set_caption("Agar.io")

# VARIÁVEIS QUE PODEM SER ALTERADAS
HOST = "IP"
PORT = 65432
text_color = (255, 255, 255)
background_color = (0, 0, 0)

# VARIÁVEIS DO SISTEMA
TINYFONT = pygame.font.Font("freesansbold.ttf", 16)
FONT = pygame.font.Font("freesansbold.ttf", 32)
BIGFONT = pygame.font.Font("freesansbold.ttf", 72)
game_over = False
counter = 0
frame_rate = 30
start_time = 0
frame_rate_delay = 0.5
droppedFrames = 0
server_uptime = 0
ping = 0
playerRadius = 25
username = ""
player_data = ""
            
# FUNÇÃO DE ANALISADOR
def parser(cell_strings, surface):
    if player_data == "":
        text = FONT.render("Algo deu errado, reconecte.", False, (255, 55, 55))
        SCREEN.blit(text, (320, 320))
    else:
        global username, playerRadius, server_uptime
        temp_string = ""
        master_strings = []
        for character in cell_strings:
            if character != "/":
                if character == "-":
                    try:
                        seconds = int(temp_string)
                        minutes = divmod(seconds, 60)[0]
                        seconds = divmod(seconds, 60)[1]
                        hours = divmod(minutes, 60)[0]
                        minutes = divmod(minutes, 60)[1]
                        server_uptime = str(int(hours)) + "h:" + str(int(minutes)) + "m:" + str(int(seconds)) + "s"
                    except ValueError:
                        pass
                    temp_string = ""
                else:
                    temp_string += str(character)
            else:
                master_strings.append(temp_string)
                temp_string = ""
        temp_string = ""
        for cell_string in master_strings:
            string_list = []
            for character in cell_string:
                if character != ":":
                    temp_string += str(character)
                else:
                    string_list.append(temp_string)
                    temp_string = ""
            try:
                temp_radius = float(string_list[0])
                temp_r = int(string_list[1])
                temp_g = int(string_list[2])
                temp_b = int(string_list[3])
                temp_x = int(string_list[4])
                temp_y = int(string_list[5])
                temp_username = string_list[6]
                if temp_username == username:
                    playerRadius = temp_radius
                if temp_username != "None":
                    pygame.draw.circle(surface, (temp_r, temp_g, temp_b), (temp_x, temp_y), int(temp_radius / 5))
                    text = TINYFONT.render(str(temp_username), False, text_color)
                    SCREEN.blit(text, (temp_x - 27.5, temp_y - 7.5))
                else:
                    pygame.draw.circle(surface, (temp_r, temp_g, temp_b), (temp_x, temp_y), int(temp_radius / 5))
            except ValueError:
                pass

# FUNÇÃO QUE ENVIA DADOS PARA O SERVIDOR
def send_to_server(message):
    global ping
    data = ""
    start_time = time.perf_counter()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.connect((HOST, PORT))
            b = message.encode("utf-8")
            s.sendall(b)
            while not data:
                data = s.recv(65536)
        except:
            return False
    if data.decode() == "GAME_OVER:" + str(username):
        return "GAME_OVER"
    ping = round((time.perf_counter() - start_time) * 1000, 1)
    return str(data.decode())

# PREPARANDO-SE PARA SE CONECTAR AO SERVIDOR E INICIAR O JOGO
while len(username) < 4 or len(username) > 8:
    username = input("Entre com seu nome de usuário: ")
    if len(username) < 4 or len(username) > 8:
        print("Seu nome de usuário deve ter no mínimo 4 e no máximo 8 letras!")
        
parser(player_data, SCREEN)
pygame.display.update()
        
send_to_server(str(username) + ":" + str(random.randint(0, 255)) + ":" + str(random.randint(0, 255)) + ":" + str(random.randint(0, 255)) + ":" + str(random.randint(0, 1280)) + ":" + str(random.randint(0, 720)) + ":")
  
# LOOP DO JOGO PRINCIPAL
while True:
    # VERIFICANDO ENTRADA
    for event in pygame.event.get():
        if event.type == QUIT:
            if game_over == False:
                send_to_server("END_CONNECTION:" + str(username) + ":")
            pygame.quit()
            sys.exit()
        if event.type == MOUSEMOTION and game_over == False:
            mouse_x, mouse_y = event.pos
        else:
            mouse_x = 640
            mouse_y = 360
    
    # COMUNICANDO COM O SERVIDOR SE O JOGO ESTIVER FUNCIONANDO
    if not game_over:
        server_data = send_to_server(str(mouse_x) + ":" + str(mouse_y) + ":" + str(username) + ":")
    
        if server_data != False:
            player_data = server_data
            parser(player_data, SCREEN)
            disconnected = False
        else:
            parser(player_data, SCREEN)
            droppedFrames += 1
            disconnected = True
    
        if player_data == "GAME_OVER":
            game_over = True
    
    # VERIFICANDO SE O JOGO ACABOU
    if game_over == True:
        text = BIGFONT.render("Você perdeu!", False, text_color)
        SCREEN.blit(text, (490, 320))
    
    # DESENHANDO A MASSA PARA A TELA
    text = FONT.render("Massa: " + str(round(playerRadius, 1)), False, text_color)
    SCREEN.blit(text, (20, 20))
    
    # COLOCANDO O PING NA TELA
    text = TINYFONT.render("Ping: " + str(ping) + "ms", False, text_color)
    SCREEN.blit(text, (20, 580))
    
    # COLOCANDO O TEMPO DE ATIVIDADE DO SERVIDOR NA TELA
    text = TINYFONT.render("Server Uptime: " + str(server_uptime), False, text_color)
    SCREEN.blit(text, (20, 600))
    
    # COLOCANDO O ENDEREÇO ​​DO SERVIDOR NA TELA
    text = TINYFONT.render("Endereço do servidor: " + str(HOST), False, text_color)
    SCREEN.blit(text, (20, 620))
    
    # COLOCANDO A QUANTIDADE DE QUADROS CAÍDOS NA TELA
    text = TINYFONT.render("Dropped Frames: " + str(droppedFrames), False, text_color)
    SCREEN.blit(text, (20, 640))
    if droppedFrames <= 50:
        text = TINYFONT.render("Sua conexão está estável", False, (55, 255, 55))
        SCREEN.blit(text, (20, 660))
    elif droppedFrames <= 100:
        text = TINYFONT.render("Sua conexão está um pouco instável", False, (155, 255, 55))
        SCREEN.blit(text, (20, 660))
    elif droppedFrames <= 300:
        text = TINYFONT.render("Sua conexão está instável, a taxa de quadros caiu", False, (255, 155, 55))
        SCREEN.blit(text, (20, 660))
        FPS = 12
    else:
        text = TINYFONT.render("Sua conexão está muito instável, caiu ainda mais a taxa de quadros", False, (255, 55, 55))
        SCREEN.blit(text, (20, 660))
        FPS = 8
    
    # COLOCANDO O STATUS DE CONEXÃO NA TELA
    if disconnected == False:
        text = TINYFONT.render("Conectado ao servidor", False, (55, 255, 55))
        SCREEN.blit(text, (20, 680))
    else:
        text = TINYFONT.render("Falha ao conectar ao servidor", False, (255, 55, 55))
        SCREEN.blit(text, (20, 680))
    
    # CALCULANDO FPS
    counter += 1
    if frame_rate > 10:
        text = FONT.render("FPS: " + str(frame_rate), False, (55, 255, 55))
        SCREEN.blit(text, (20, 60))
    elif frame_rate <= 10 and frame_rate > 5:
        text = FONT.render("FPS: " + str(frame_rate), False, (255, 255, 55))
        SCREEN.blit(text, (20, 60))
    elif frame_rate <= 5 and frame_rate > 0:
        text = FONT.render("FPS: " + str(frame_rate), False, (255, 55, 55))
        SCREEN.blit(text, (20, 60))
        
    if (time.time() - start_time) > frame_rate_delay:
        frame_rate = round(counter / (time.time() - start_time))
        counter = 0
        start_time = time.time()
    
    # FINALIZANDO AS COISAS
    WIDTH, HEIGHT = pygame.display.get_surface().get_size()
    pygame.display.update()
    CLOCK.tick(FPS)
    SCREEN.fill(background_color)
    SCREEN.blit(fundo,(0, 0))
